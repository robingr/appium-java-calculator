package exercises;

import framework.AppiumTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CalculatorTest extends AppiumTest {

    @Test
    void should_multiply_two_and_two() {
        calculatorScreen.clickNumber(2);
        calculatorScreen.clickMultiply();
        calculatorScreen.clickNumber(2);
        calculatorScreen.clickEquals();

        assertThat(calculatorScreen.getResult())
                .as("Result of two times two should be four.")
                .isEqualTo(4);
    }

    @Test
    void should_divide_eight_by_four() {

        // Write the code for exercise two here

        // Don't forget the assertion!
    }

}
