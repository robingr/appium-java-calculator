package framework;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.remote.DesiredCapabilities;
import pages.CalculatorScreen;

import java.io.IOException;
import java.net.URL;

public class AppiumTest {

    private static AppiumDriver<MobileElement> driver;
    protected CalculatorScreen calculatorScreen;

    @BeforeAll
    static void setUp() throws IOException, InterruptedException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("automationName", Configuration.getProperty("DRIVER_NAME"));
        capabilities.setCapability("deviceName", Configuration.getProperty("DEVICE_ID"));
        capabilities.setCapability("udid", Configuration.getProperty("DEVICE_ID"));
        capabilities.setCapability("appPackage", Configuration.getProperty("APP_NAME"));
        capabilities.setCapability("appActivity", Configuration.getProperty("APP_ACTIVITY"));

        driver = new AndroidDriver<>(new URL(Configuration.getProperty("APPIUM_SERVER_URL")), capabilities);
        Thread.sleep(2000); // Wait for the app to be ready (sometimes, the advanced pad is expanded on start)
    }

    @BeforeEach
    void testSetUp() {
        calculatorScreen = new CalculatorScreen(driver);
    }

    @AfterEach
    void testTearDown() {

    }

    @AfterAll
    static void tearDown() {
        driver.quit();
    }

}
